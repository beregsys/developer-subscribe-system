<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\NewSubscriberNotification;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::with('position')->paginate(15);
//        dd($subscribers);
        return response($subscribers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateData($request);
        try {
            $subscriber = Subscriber::create($validatedData);
        } catch (\Exception $error) {
            return response(['message' => $error->getMessage()]);
        }


        Mail::to('a963c92055-1201c2@inbox.mailtrap.io')->send(new NewSubscriberNotification($subscriber));


        return response(['message' => 'Subscriber saved successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validatedData = $this->validateData($request, false);
            Subscriber::find($id)->update($validatedData);
        } catch (\Exception $error) {
            return response(['message' => $error->getMessage()]);
        }

        return response(['message' => 'Subscriber updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Subscriber::find($id)->delete();
        } catch (\Exception $error) {
            return response(['message' => $error->getMessage()]);
        }

        return response(['message' => 'Subscriber deleted successfully']);

    }

    private function validateData(Request $request, $emailUnique = true) {

        $unique = ($emailUnique) ? '|unique:subscribers' : '';

        return $request->validate([
            'name' => 'required|string|max:30|min:2',
            'surname' => 'required|string|max:30|min:2',
            'email' => 'required|email:rfc,dns' . $unique,
            'description' => 'string',
            'position_id' => 'integer',
            'field_one' => 'string|nullable',
            'field_two' => 'string|nullable',
            'field_three' => 'integer',
            'field_three_value' => 'string|nullable'
        ]);
    }
}
