<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = 'subscribers';
    protected $fillable = [
        'name',
        'surname',
        'email',
        'description',
        'position_id',
        'field_one',
        'field_two',
        'field_three',
        'field_three_value'
    ];

    public function position()
    {
        return $this->belongsTo(Position::class);
    }
}
