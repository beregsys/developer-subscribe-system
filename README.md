## Developer subscribe system

Install project
1. Clone repository
2. Make sure you have installed php, mysql, node, npm
3. After cloning repository, navigate to project folder
    ```
    cd developer-subscribe-system
    ```
4. Install composer packages:
    ```
    composer install
    ```
5. Install front packages:
    ```
   npm install && npm run prod
   ```
6. Open .env.example and rename it to .env, then add this configuration
    ```
    DB_DATABASE="your_database name"
    DB_USERNAME="your_user_name"
    DB_PASSWORD="your_database_password'
    ```
7. Then execute migration with seeders.
    ```
    php artisan migrate --seed
    ```
8. Laravel key generation
    ```
   php artisan key:generate
   ```
9. Mail configuration - I've used mailtrap for testing so if you have account you can add this data to .env file here:
    ```
    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME="your_user_name"
    MAIL_PASSWORD="your_password"
    MAIL_ENCRYPTION=tls
    MAIL_FROM_ADDRESS=null
    MAIL_FROM_NAME="${APP_NAME}"
   ```
10. In this application you have user and admin area(route - /admin/dashboard), you can login with admin account:
    ```
    login - admin@example.com
    password - admin
    ```
11. After adding some subscribers you can edit and delete it in dashboard.

12. And in the end run this: 
    ```
    php artisan serve
    ```
    ```
    :)
    ```
