<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribersTable extends Migration
{
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->text('description')->nullable();
            $table->tinyInteger('position_id')->default(0);
            $table->string('field_one', 100)->nullable();
            $table->string('field_two', 100)->nullable();
            $table->tinyInteger('field_three')->default(0);
            $table->string('field_three_value', 100)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
