<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Tester'],
            ['name' => 'Developer'],
            ['name' => 'Project Manager']
        ];
        foreach ($data as $position) {
            Position::create($position);
        }

    }
}
