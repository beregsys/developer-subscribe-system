@component('mail::message')
    Hello **{{$subscriber->name}}**  
    Your data:  
        {{$subscriber->name ?? ''}}  
        {{$subscriber->surname ?? ''}}  
        {{$subscriber->email ?? ''}}  
        {{$subscriber->position->name ?? ''}}  
        {{$subscriber->field_one ?? ''}}  
        {{$subscriber->field_two ?? ''}}  
        {{$subscriber->field_three_value ?? ''}}  
@endcomponent
