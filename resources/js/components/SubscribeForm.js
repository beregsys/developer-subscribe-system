import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

class SubscribeForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: 0,
            formStatus: ''
        };

        this.change = this.change.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    change(event) {
        this.setState({
            checked: event.target.value * 1
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        let data = {
            name: event.target.name.value,
            surname: event.target.surname.value,
            email: event.target.email.value,
            description: event.target.description.value,
            position_id: parseInt(event.target.position_id.value) + 1,
            field_one: event.target.field_one.value,
            field_two: event.target.field_two.value,
            field_three: event.target.field_three.checked ? 1 : 0,
            field_three_value: event.target.field_three.checked ? event.target.field_three.value : ''
        }
        await axios({
            url: '/subscribers',
            method: 'POST',
            data: data
        })
            .then( response => {
                if(response.status === 200) {
                    this.setState({
                        formStatus: response.data.message
                    })
                }
            })
            .catch(e => console.log(e))
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Subscribe Form</div>

                            <div className="card-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="name">Imie</label>
                                        <input type="text" className="form-control" id="name" name="name" required />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="surname">Nazwisko</label>
                                        <input type="text" className="form-control" id="surname" name="surname" required />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input type="email" className="form-control" id="email" name="email" required />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="description">Opis</label>
                                        <textarea className="form-control" id="description" rows="3" name="description" />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="position">Stanowisko</label>
                                        <select
                                            className="form-control"
                                            id="position"
                                            onChange={this.change}
                                            value={this.state.checked}
                                            name="position_id"
                                        >
                                            <option value="0">Tester</option>
                                            <option value="1">Developer</option>
                                            <option value="2">Project Manager</option>
                                        </select>
                                    </div>

                                    {this.state.checked === 0 &&
                                        <div>
                                            <div className="form-group">
                                                <label className="d-block">Systemy testujące
                                                    <input type="text" className="form-control" name="field_one" />
                                                </label>
                                            </div>
                                            <div className="form-group">
                                                <label className="d-block">Systemy raportowe
                                                    <input type="text" className="form-control" name="field_two" />
                                                </label>
                                            </div>
                                            <div className="form-group form-check">
                                                <label className="form-check-label">
                                                    <input type="checkbox" className="form-check-input" name="field_three" value="Zna selenium" />
                                                    Zna selenium
                                                </label>
                                            </div>
                                        </div>

                                    }
                                    {this.state.checked === 1 &&
                                        <div>
                                            <div className="form-group">
                                                <label className="d-block">Środowiska ide
                                                    <input type="text" className="form-control" name="field_one" />
                                                </label>
                                            </div>
                                            <div className="form-group">
                                                <label className="d-block">Języki programowania
                                                    <input type="text" className="form-control" name="field_two" />
                                                </label>
                                            </div>
                                            <div className="form-group form-check">
                                                <label className="form-check-label">
                                                    <input type="checkbox" className="form-check-input" name="field_three" value="Zna mysql" />
                                                    Zna mysql
                                                </label>
                                            </div>
                                        </div>
                                    }
                                    {this.state.checked === 2 &&
                                    <div>
                                        <div className="form-group">
                                            <label className="d-block">Metodologie prowadzenia projektów
                                                <input type="text" className="form-control" name="field_one" />
                                            </label>
                                        </div>
                                        <div className="form-group">
                                            <label className="d-block">Systemy raportowe
                                                <input type="text" className="form-control" name="field_two" />
                                            </label>
                                        </div>
                                        <div className="form-group form-check">
                                            <label className="form-check-label">
                                                <input type="checkbox" className="form-check-input" name="field_three" value="Zna scrum" />
                                                Zna scrum
                                            </label>
                                        </div>
                                    </div>
                                    }

                                    <button type="submit" className="btn btn-primary">Submit</button>
                                </form>

                                {this.state.formStatus &&
                                    <div className="alert alert-success mt-3" role="alert">
                                        {this.state.formStatus}
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SubscribeForm;

if (document.getElementById('subscribeForm')) {
    ReactDOM.render(<SubscribeForm />, document.getElementById('subscribeForm'));
}
