import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Modal, { ModalHeader, ModalBody, ModalFooter } from "./Modal";



class SubscriberList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            modal: false,
            checked: 0,
            popupData: false
        }

        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.toggle = this.toggle.bind(this);
        this.change = this.change.bind(this);
        this.handleChangeField = this.handleChangeField.bind(this);
        this.saveSubscriber = this.saveSubscriber.bind(this);
        this.getData = this.getData.bind(this);
    }

    toggle() {
        this.setState({ modal: !this.state.modal });
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        axios({
            url: '/subscribers',
            method: 'GET',
        })
            .then( response => {
                if(response.status === 200) {
                    this.setState({
                        data: response.data.data,
                        subscriberID: response.data.data[0].id
                    })
                }
            })
            .catch(e => console.log(e))
    }

    handleEditClick(state) {
        this.toggle();
        this.state.data.forEach((data) => {
            if (data.id === parseInt(state.target.id)) {
                this.setState({
                    popupData: data,
                    checked: data.position_id - 1
                });
            }
        });
    }

    async handleDeleteClick(state) {
        await axios({
            url: '/subscribers/' + state.target.id,
            method: 'DELETE',
        })
            .then( response => {
                if(response.status === 200) {
                    this.getData();
                }
            })
            .catch(e => console.log(e))
    }

    change(event) {
        let popupData = this.state.popupData;
        popupData[event.target.name] = parseInt(event.target.value) + 1 ;
        this.setState({
            checked: event.target.value * 1,
            popupData: popupData
        });
    }

    handleChangeField(event) {
        let popupData = this.state.popupData;
        if(event.target.name === 'field_three') {
            popupData[event.target.name] = +!popupData[event.target.name];
            this.setState({
                popupData: popupData
            });
        } else {
            popupData[event.target.name] = event.target.value;
            this.setState({
                popupData: popupData
            })
        }
    }

    async saveSubscriber() {
        await axios({
            url: '/subscribers/' + this.state.popupData.id,
            method: 'PUT',
            data: this.state.popupData
        })
            .then( response => {
                if(response.status === 200) {
                    this.toggle();
                    this.getData();
                }
            })
            .catch(e => console.log(e))
    }

    render() {
        const columns = [
            {
                name: 'ID',
                selector: 'id'
            },
            {
                name: 'Name',
                selector: 'name'
            },
            {
                name: 'Surname',
                selector: 'surname'
            },
            {
                name: 'Email',
                selector: 'email'
            },
            {
                name: 'Position',
                selector: 'position.name'
            },
            {
                cell:(row)=><div className="actions"><button className="btn btn-success btn-sm mx-1" onClick={this.handleEditClick} id={row.id}>Edit</button><button className="btn btn-danger btn-sm mx-1" onClick={this.handleDeleteClick} id={row.id}>Delete</button></div>,
                ignoreRowClick: true,
            },
        ];


        return (

            <div>
                <DataTable
                    title="Subscribers"
                    columns={columns}
                    data={this.state.data}
                />
                {this.state.popupData &&
                    <Modal isOpen={this.state.modal}>
                    <ModalHeader>
                        <h3>Edit Subscriber</h3>
                        <button
                            type="button"
                            className="close"
                            aria-label="Close"
                            onClick={this.toggle}
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </ModalHeader>
                    <ModalBody>

                        <div className="form-group">
                            <label htmlFor="name">Imie</label>
                            <input
                                type="text"
                                className="form-control"
                                id="name"
                                name="name"
                                value={this.state.popupData.name}
                                onChange={this.handleChangeField}
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="surname">Nazwisko</label>
                            <input
                                type="text"
                                className="form-control"
                                id="surname"
                                name="surname"
                                value={this.state.popupData.surname}
                                onChange={this.handleChangeField}
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input
                                type="email"
                                className="form-control"
                                id="email"
                                name="email"
                                value={this.state.popupData.email}
                                onChange={this.handleChangeField}
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="description">Opis</label>
                            <textarea
                                className="form-control"
                                id="description"
                                rows="3"
                                name="description"
                                value={this.state.popupData.description}
                                onChange={this.handleChangeField}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="position">Stanowisko</label>
                            <select
                                className="form-control"
                                id="position"
                                onChange={this.change}
                                value={this.state.checked}
                                name="position_id"
                            >
                                <option value="0">Tester</option>
                                <option value="1">Developer</option>
                                <option value="2">Project Manager</option>
                            </select>
                        </div>

                        {this.state.checked === 0 &&
                        <div>
                            <div className="form-group">
                                <label className="d-block">Systemy testujące
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="field_one"
                                        value={this.state.popupData.field_one || ''}
                                        onChange={this.handleChangeField}
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label className="d-block">Systemy raportowe
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="field_two"
                                        value={this.state.popupData.field_two || ''}
                                        onChange={this.handleChangeField}
                                    />
                                </label>
                            </div>
                            <div className="form-group form-check">
                                <label className="form-check-label">
                                    <input
                                        type="checkbox"
                                        className="form-check-input"
                                        name="field_three"
                                        value="Zna selenium"
                                        checked={this.state.popupData.field_three}
                                        onChange={this.handleChangeField}
                                    />
                                    Zna selenium
                                </label>
                            </div>
                        </div>

                        }
                        {this.state.checked === 1 &&
                        <div>
                            <div className="form-group">
                                <label className="d-block">Środowiska ide
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="field_one"
                                        value={this.state.popupData.field_one || ''}
                                        onChange={this.handleChangeField}
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label className="d-block">Języki programowania
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="field_two"
                                        value={this.state.popupData.field_two || ''}
                                        onChange={this.handleChangeField}
                                    />
                                </label>
                            </div>
                            <div className="form-group form-check">
                                <label className="form-check-label">
                                    <input
                                        type="checkbox"
                                        className="form-check-input"
                                        name="field_three"
                                        value="Zna mysql"
                                        checked={this.state.popupData.field_three}
                                        onChange={this.handleChangeField}
                                    />
                                    Zna mysql
                                </label>
                            </div>
                        </div>
                        }
                        {this.state.checked === 2 &&
                        <div>
                            <div className="form-group">
                                <label className="d-block">Metodologie prowadzenia projektów
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="field_one"
                                        value={this.state.popupData.field_one || ''}
                                        onChange={this.handleChangeField}
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label className="d-block">Systemy raportowe
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="field_two"
                                        value={this.state.popupData.field_two || ''}
                                        onChange={this.handleChangeField}
                                    />
                                </label>
                            </div>
                            <div className="form-group form-check">
                                <label className="form-check-label">
                                    <input
                                        type="checkbox"
                                        className="form-check-input"
                                        name="field_three"
                                        checked={this.state.popupData.field_three}
                                        onChange={this.handleChangeField}
                                    />
                                    Zna scrum
                                </label>
                            </div>
                        </div>
                        }
                    </ModalBody>
                    <ModalFooter>
                        <button
                            type="button"
                            className="btn btn-secondary"
                            onClick={this.toggle}
                        >
                            Close
                        </button>
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={this.saveSubscriber}
                        >
                            Save changes
                        </button>
                    </ModalFooter>
                </Modal>
                }
            </div>

        );
    }

}

export default SubscriberList;

if (document.getElementById('SubscriberList')) {
    ReactDOM.render(<SubscriberList {...props} />, document.getElementById('SubscriberList'));
}
